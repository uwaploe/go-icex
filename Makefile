#
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

GOBUILD := go build -i
APPS := $(notdir $(wildcard cmd/*))
PKG := bitbucket.org/uwaploe/go-icex
CMDDIRS := $(wildcard cmd/*)

.PHONY: all images

all: $(APPS)

images: ## Build all Docker images
	for dir in $(CMDDIRS); do \
	    test -e $$dir/Makefile && $(MAKE) -C $$dir image; \
	done

dep:
	@go get -v -d ./...

$(APPS): dep
	@$(GOBUILD) -v -o $@ \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
	  $(PKG)/cmd/$@

clean:
	@rm -f $(APPS)
