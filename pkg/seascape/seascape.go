// Package seascape contains functions for working with Seascape data packets
package seascape

import (
	"fmt"
	"strconv"
	"time"

	"strings"

	"bitbucket.org/mfkenney/go-nmea"
	"github.com/pkg/errors"
)

const (
	Tick       time.Duration = time.Microsecond * 10
	DoubletGap               = time.Millisecond * 35
)

const RECORDS_PER_PACKET uint = 4
const MISSING_VALUE int = -1

var NoSignal Detection = Detection{-1, -1, -1}

// Acoustic detection information
type Detection struct {
	// Timer value in ticks since the last second
	Dt int
	// Signal to noise ratio in dB
	Snr int
	// Pulse width in ticks
	Pw int
}

// Timer packet for input to Seascape
type TimerPacket struct {
	// Time-stamp (1 second resolution)
	t time.Time
	// Origin latitude in degrees
	lat float64
	// Origin longitude in degrees
	lon float64
	// Grid azimuth in degrees
	azimuth float64
	// Detection records
	data []*TimerRecord
	// Record count
	nrecs int
}

// Timer data record
type TimerRecord struct {
	// Hydrophone ID
	Id uint
	// Time-stamp (1 second resolution)
	T time.Time
	// Hydrophone fix quality
	Fix string
	// Latitude in degrees
	Lat float64
	// Longitude in degrees
	Lon float64
	// Sub-band detections
	Bands [4]Detection
	// Framing pulse detection
	Framing Detection
}

func todm(d float64) (int, float64) {
	deg := int(d)
	min := (d - float64(deg)) * 60
	return deg, min
}

func (d *Detection) parseFromStrings(vals []string) error {
	val, err := strconv.ParseInt(vals[0], 10, 32)
	if err != nil {
		return err
	}
	d.Dt = int(val)

	val, err = strconv.ParseInt(vals[1], 10, 32)
	if err != nil {
		fval, err := strconv.ParseFloat(vals[1], 32)
		if err != nil {
			return err
		}
		d.Snr = int(fval)
	} else {
		d.Snr = int(val)
	}

	val, err = strconv.ParseInt(vals[2], 10, 32)
	if err != nil {
		return err
	}
	d.Pw = int(val)

	return nil
}

// MarshalReduced packs a TimerRecord into a reduced text format record
func (tr *TimerRecord) MarshalReduced() ([]byte, error) {
	fields := make([]string, 17)
	fields[0] = strconv.FormatUint(uint64(tr.Id), 10)
	fields[1] = strconv.FormatInt(tr.T.Unix(), 10)

	var (
		i int
		d *Detection
	)

	i = 2 // field index
	for j := 0; j < 4; j++ {
		d = &tr.Bands[j]
		fields[i] = strconv.Itoa(d.Dt)
		fields[i+1] = strconv.Itoa(d.Snr)
		fields[i+2] = strconv.Itoa(d.Pw)
		i += 3
	}

	fields[i] = strconv.Itoa(tr.Framing.Dt)
	fields[i+1] = strconv.Itoa(tr.Framing.Snr)
	fields[i+2] = strconv.Itoa(tr.Framing.Pw)

	return []byte(strings.Join(fields, ",")), nil
}

// UnmarshalReduced unpacks the structure contents from a reduced text
// text format containing only the detection data.
func (tr *TimerRecord) UnmarshalReduced(text []byte) error {
	fields := strings.Split(string(text), ",")
	if len(fields) < 17 {
		return errors.New("Invalid record length")
	}

	uval, err := strconv.ParseUint(fields[0], 10, 32)
	if err != nil {
		return errors.Wrap(err, "parsing ID field")
	}
	tr.Id = uint(uval)

	val, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		return errors.Wrap(err, "parsing timestamp")
	}
	tr.T = time.Unix(val, 0).UTC()

	var (
		i int
		d *Detection
	)

	i = 2 // field index
	for j := 0; j < 4; j++ {
		d = &tr.Bands[j]
		err = d.parseFromStrings(fields[i : i+3])
		if err != nil {
			return errors.Wrapf(err, "sub-band %d", j+1)
		}
		i += 3
	}

	d = &tr.Framing
	err = d.parseFromStrings(fields[i : i+3])
	if err != nil {
		return errors.Wrap(err, "framing detection")
	}

	return nil
}

// MarshalNmea packs the structure into an NMEA sentence
func (tr *TimerRecord) MarshalNmea() (*nmea.Sentence, error) {
	s := nmea.Sentence{}
	s.Fields = make([]string, 21)
	s.Id = fmt.Sprintf("NODE_%d", tr.Id)
	s.Fields[0] = tr.T.Truncate(time.Second).Format("150405")
	s.Fields[1] = tr.Fix
	d, m := todm(tr.Lat)
	if d < 0 || m < 0 {
		s.Fields[2] = fmt.Sprintf("%02d%07.4f", -d, -m)
		s.Fields[3] = "S"
	} else {
		s.Fields[2] = fmt.Sprintf("%02d%07.4f", d, m)
		s.Fields[3] = "N"
	}
	d, m = todm(tr.Lon)
	if d < 0 || m < 0 {
		s.Fields[4] = fmt.Sprintf("%03d%07.4f", -d, -m)
		s.Fields[5] = "W"
	} else {
		s.Fields[4] = fmt.Sprintf("%03d%07.4f", d, m)
		s.Fields[5] = "E"
	}

	j := int(6)
	for i := 0; i < 4; i++ {
		s.Fields[j] = strconv.FormatInt(int64(tr.Bands[i].Dt), 10)
		j++
		s.Fields[j] = strconv.FormatInt(int64(tr.Bands[i].Snr), 10)
		j++
		s.Fields[j] = strconv.FormatInt(int64(tr.Bands[i].Pw), 10)
		j++
	}

	s.Fields[j] = strconv.FormatInt(int64(tr.Framing.Dt), 10)
	j++
	s.Fields[j] = strconv.FormatInt(int64(tr.Framing.Snr), 10)
	j++
	s.Fields[j] = strconv.FormatInt(int64(tr.Framing.Pw), 10)

	return &s, nil
}

// EmptyNmea creates an NMEA sentence for a missing node data record
func EmptyNmea(nodeid int, t time.Time) *nmea.Sentence {
	s := nmea.Sentence{}
	s.Fields = make([]string, 21)
	s.Id = fmt.Sprintf("NODE_%d", nodeid)
	s.Fields[0] = t.Truncate(time.Second).Format("150405")
	s.Fields[1] = "M"
	return &s
}

// Time returns the absolute detection time for the specified sub-band
func (tr *TimerRecord) Time(band int) (t time.Time) {
	if band >= 0 && band < len(tr.Bands) {
		if dt := tr.Bands[band].Dt; dt >= 0 {
			t = tr.T.Add(Tick * time.Duration(dt))
		}
	}

	if band == len(tr.Bands) {
		if dt := tr.Framing.Dt; dt >= 0 {
			t = tr.T.Add(Tick * time.Duration(dt))
		}
	}

	return
}

// NewPacket creates a new TimerPacket struct
func NewPacket() *TimerPacket {
	tp := TimerPacket{}
	tp.data = make([]*TimerRecord, RECORDS_PER_PACKET)
	tp.nrecs = 0
	return &tp
}

// Len returns the record count for a TimerPacket
func (tp *TimerPacket) Len() int {
	return tp.nrecs
}

// Reset discards all records and zeros the timestamp.
func (tp *TimerPacket) Reset() {
	tp.t = time.Time{}
	tp.data = make([]*TimerRecord, RECORDS_PER_PACKET)
	tp.nrecs = 0
}

// AddRecord adds a new TimerRecord to a TimerPacket and returns false if
// the record could not be added which either means the packet is full or
// the record timestamp doesn't match the packet timestamp.
func (tp *TimerPacket) AddRecord(tr *TimerRecord) bool {
	idx := tr.Id - 1
	if (tp.nrecs == 0 || tp.t.Equal(tr.T)) && idx >= 0 && idx < RECORDS_PER_PACKET {
		tp.t = tr.T
		tp.data[idx] = tr
		tp.nrecs += 1
		return true
	}

	return false
}

// AddGrid adds the ICEX grid information to a TimerPacket
func (tp *TimerPacket) AddGrid(lat, lon, azimuth float64) {
	tp.lat = lat
	tp.lon = lon
	tp.azimuth = azimuth
}

// MarshalText implements the encoding.TextMarshaler interface
func (tp *TimerPacket) MarshalText() ([]byte, error) {
	contents := make([]string, 0, 2+len(tp.data))

	rmc := &nmea.Sentence{}
	rmc.Fields = make([]string, 11)
	rmc.Id = "GPRMC"
	rmc.Fields[0] = tp.t.Truncate(time.Second).Format("150405")
	rmc.Fields[1] = "A"

	d, m := todm(tp.lat)
	if d < 0 || m < 0 {
		rmc.Fields[2] = fmt.Sprintf("%02d%07.4f", -d, -m)
		rmc.Fields[3] = "S"
	} else {
		rmc.Fields[2] = fmt.Sprintf("%02d%07.4f", d, m)
		rmc.Fields[3] = "N"
	}
	d, m = todm(tp.lon)
	if d < 0 || m < 0 {
		rmc.Fields[4] = fmt.Sprintf("%03d%07.4f", -d, -m)
		rmc.Fields[5] = "W"
	} else {
		rmc.Fields[4] = fmt.Sprintf("%03d%07.4f", d, m)
		rmc.Fields[5] = "E"
	}
	rmc.Fields[8] = tp.t.Truncate(time.Second).Format("020106")
	contents = append(contents, rmc.String())

	hdt := &nmea.Sentence{}
	hdt.Fields = make([]string, 2)
	hdt.Id = "GPHDT"
	hdt.Fields[0] = fmt.Sprintf("%05.1f", tp.azimuth)
	hdt.Fields[1] = "T"
	contents = append(contents, hdt.String())

	var (
		s   *nmea.Sentence
		err error
	)

	for i, rec := range tp.data {
		if rec != nil {
			s, err = rec.MarshalNmea()
			if err != nil {
				return nil, errors.Wrapf(err, "encoding detection record %d", i)
			}
		} else {
			s = EmptyNmea(i+1, tp.t)
		}
		contents = append(contents, s.String())
	}
	// Append an empty string so the strings.Join output will end with
	// a CR-LF.
	contents = append(contents, "")

	return []byte(strings.Join(contents, "\r\n")), nil
}
