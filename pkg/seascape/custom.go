package seascape

import (
	"bytes"
	"strconv"
	"time"
)

// CustomOutput encapsulates the custom fix output from Seascape
type CustomOutput struct {
	T         time.Time
	Name      string
	Latitude  float64
	Longitude float64
}

// UnmarshalText implements the encoding.TextUnmarshaler interface
func (o *CustomOutput) UnmarshalText(text []byte) error {
	var err error

	fields := bytes.Split(bytes.TrimRight(text, "\r\n"), []byte(","))
	o.Name = string(fields[0])
	if o.Latitude, err = strconv.ParseFloat(string(fields[3]), 64); err != nil {
		return err
	}
	if o.Longitude, err = strconv.ParseFloat(string(fields[4]), 64); err != nil {
		return err
	}

	ts := bytes.Join(fields[1:3], []byte(" "))
	if o.T, err = time.Parse("2006-01-02 15:04:05", string(ts)); err != nil {
		return err
	}
	o.T = o.T.UTC()

	return nil
}
