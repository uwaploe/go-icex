package seascape

import (
	"testing"
	"time"
)

func TestRecordOutput(t *testing.T) {
	tp := TimerRecord{
		Id:  1,
		T:   time.Date(2016, time.December, 17, 16, 59, 39, 0, time.UTC),
		Lat: 47.655200,
		Lon: -122.3168834,
		Fix: "A",
		Bands: [4]Detection{
			NoSignal,
			NoSignal,
			NoSignal,
			NoSignal,
		},
		Framing: NoSignal,
	}
	s, _ := tp.MarshalNmea()
	if s.Id != "NODE_1" {
		t.Errorf("Bad sentence ID: %q", s.Id)
	}

	if s.Fields[0] != "165939" {
		t.Errorf("Bad time field: %q", s.Fields[0])
	}

	if s.Fields[4] != "12219.0130" || s.Fields[5] != "W" {
		t.Errorf("Bad longitude format: %q, %q", s.Fields[4], s.Fields[5])
	}

}

func TestRecordInput(t *testing.T) {
	rec := []byte("2,1450372480,-1,-1,-1,1234,42.5,100,-1,-1,-1,7890,32,98,-1,-1,-1,")
	t_expect := time.Date(2015, time.December, 17, 17, 14, 40, 0, time.UTC)

	tr := &TimerRecord{}
	err := tr.UnmarshalReduced(rec)
	if err != nil {
		t.Fatal(err)
	}

	if !tr.T.Equal(t_expect) {
		t.Errorf("Time mismatch: %v != %v", tr.T, t_expect)
	}

	if tr.Bands[1].Dt != 1234 {
		t.Errorf("Bad detection value: %d != 1234", tr.Bands[1].Dt)
	}

	// Verify conversion from float to int.
	if tr.Bands[1].Snr != 42 {
		t.Errorf("Bad snr value: %d != 1234", tr.Bands[1].Snr)
	}

	if tr.Framing.Dt != -1 {
		t.Errorf("Bad detection value: %d != -1", tr.Framing.Dt)
	}

}

func TestReducedReadWrite(t *testing.T) {
	rec := []byte("2,1450372480,-1,-1,-1,1234,42,100,-1,-1,-1,7890,32,98,-1,-1,-1")

	tr := &TimerRecord{}
	err := tr.UnmarshalReduced(rec)
	if err != nil {
		t.Fatal(err)
	}

	rec2, err := tr.MarshalReduced()
	if err != nil {
		t.Fatal(err)
	}

	if string(rec2) != string(rec) {
		t.Errorf("%q != %q", rec2, rec)
	}
}

func TestPacketBuild(t *testing.T) {
	recs := [][]byte{
		[]byte("2,1450372480,-1,-1,-1,1234,42,100,-1,-1,-1,7890,32,98,-1,-1,-1"),
		[]byte("1,1450372480,-1,-1,-1,1234,42,100,-1,-1,-1,7890,32,98,-1,-1,-1"),
		[]byte("3,1450372480,-1,-1,-1,1234,42,100,-1,-1,-1,7890,32,98,-1,-1,-1"),
		[]byte("4,1450372480,-1,-1,-1,1234,42,100,-1,-1,-1,7890,32,98,-1,-1,-1"),
	}

	tp := NewPacket()
	if tp.Len() != 0 {
		t.Errorf("Bad packet length: %d", tp.Len())
	}

	for i, rec := range recs {
		tr := &TimerRecord{}
		err := tr.UnmarshalReduced(rec)
		if err != nil {
			t.Fatal(err)
		}
		if !tp.AddRecord(tr) {
			t.Error("Cannot add record")
		}

		if tp.Len() != (i + 1) {
			t.Errorf("Bad packet length: got %d, expected %d", tp.Len(), i+1)
		}
	}

	rec := []byte("2,1450372481,-1,-1,-1,1234,42,100,-1,-1,-1,7890,32,98,-1,-1,-1")
	tr := &TimerRecord{}
	err := tr.UnmarshalReduced(rec)
	if err != nil {
		t.Fatal(err)
	}

	if tp.AddRecord(tr) {
		t.Errorf("Added record with bad timestamp: %v != %v", tr.T, tp.t)
	}

	tp.Reset()
	tp.AddRecord(tr)
	if tp.Len() != 1 {
		t.Errorf("Packet not reset: Len() == %d", tp.Len())
	}

}

func TestCustom(t *testing.T) {
	input := []byte("TORPEDO-1,2017-11-30,19:19:12,47.654948,-122.316775,\r\n")
	rec := CustomOutput{}
	if err := rec.UnmarshalText(input); err != nil {
		t.Fatal(err)
	}

	expect := CustomOutput{
		Name:      "TORPEDO-1",
		T:         time.Date(2017, time.November, 30, 19, 19, 12, 0, time.UTC),
		Latitude:  47.654948,
		Longitude: -122.316775,
	}

	if rec != expect {
		t.Errorf("%+v != %+v", rec, expect)
	}
}
