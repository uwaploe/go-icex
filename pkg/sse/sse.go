// Package sse implements HTTP Server Sent Events. It supports the http.Handler
// interface to allow it to be used with http.ServeMux:
//
//    mux := http.NewServeMux()
//    b := NewBroker()
//    mux.Handle("/stream", b)
//
package sse

import (
	"fmt"
	"log"
	"net/http"
)

type Event struct {
	// Event name
	Name []byte
	// Event data
	Data []byte
}

func (ev Event) String() string {
	return fmt.Sprintf("event: %s\ndata: %s\n\n", ev.Name, ev.Data)
}

type Broker struct {
	// Channel to receive Events which will be forwarded
	// to all registered clients.
	Notifier chan Event
	// New client connections
	newClient chan chan Event
	// Closed connections
	closeClient chan chan Event
	// Client registry
	registry map[chan Event]bool
}

func (b *Broker) listen() {
	for {
		select {
		case c := <-b.newClient:
			b.registry[c] = true
			log.Printf("Client registered [%d]", len(b.registry))
		case c := <-b.closeClient:
			delete(b.registry, c)
			log.Printf("Client removed [%d]", len(b.registry))
		case ev := <-b.Notifier:
			for ch, _ := range b.registry {
				ch <- ev
			}
		}
	}
}

// ServeHttp implements the http.Handler interface.
func (b *Broker) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	// Flushing must be supported so we can stream the response.
	flusher, ok := rw.(http.Flusher)
	if !ok {
		http.Error(rw, "Streaming unsupported!", http.StatusInternalServerError)
		return
	}

	// Set the headers related to event streaming.
	rw.Header().Set("Content-Type", "text/event-stream")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
	rw.Header().Set("Access-Control-Allow-Origin", "*")

	// Register the new client and arrange to unregister at function exit
	ch := make(chan Event)
	b.newClient <- ch
	defer func() {
		b.closeClient <- ch
	}()

	// Listen for connection close event and unregister the client
	// if that occurs.
	closing := rw.(http.CloseNotifier).CloseNotify()
	go func() {
		<-closing
		b.closeClient <- ch
	}()

	// Wait for Events on our client channel and forward them over the HTTP
	// link in SSE format.
	for ev := range ch {
		fmt.Fprintf(rw, "%s", ev)
		flusher.Flush()
	}
}

// NewBroker creates a new Broker and starts it listening for Events
func NewBroker() *Broker {
	b := &Broker{
		Notifier:    make(chan Event, 1),
		newClient:   make(chan chan Event),
		closeClient: make(chan chan Event),
		registry:    make(map[chan Event]bool),
	}

	go b.listen()

	return b
}
