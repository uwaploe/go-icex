package dirs

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

var DATA = "17/12/2015 17:14:40\t407\t1067\t1057\t400\t-1\t-1\t-1\t-1\t6733\t5667\t6080\t3463\t-1\t-1\t-1\t-1"

func TestIo(t *testing.T) {
	d := &Detection{}
	err := d.UnmarshalText([]byte(DATA))
	if err != nil {
		t.Fatal(err)
	}

	if d.T.Hour() != 17 {
		t.Fatalf("Bad time decode: %v", d.T)
	}

	if (d.F1q1())[3] != 400 || (d.F2q1())[0] != 6733 || (d.F2q2())[3] != -1 {
		t.Fatalf("Decode error: %v %v", d.F1q1(), d.F2q2())
	}

	output, err := d.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	if string(output) != DATA {
		t.Fatalf("Output mismatch: %q != %q", string(output), DATA)
	}

}

func TestSelector(t *testing.T) {
	d := &Detection{}
	err := d.UnmarshalText([]byte(DATA))
	if err != nil {
		t.Fatal(err)
	}

	dt := d.Time(1, 1, 2)
	ticks := dt.Sub(d.T)
	if ticks != (Tick * 1067) {
		t.Errorf("Unexpected tick value: %v != %v", ticks, Tick*1067)
	}

	dt = d.Time(0, 0, 0)
	if !dt.IsZero() {
		t.Errorf("Bad selection index not caught")
	}

	ticks2 := d.Ticks(1, 2, 2)
	if ticks2 != -1 {
		t.Errorf("missing value not detected (%d)", ticks2)
	}
}

func TestWindow(t *testing.T) {
	d := &Detection{}
	err := d.UnmarshalText([]byte(DATA))
	if err != nil {
		t.Fatal(err)
	}

	if d.IsOdd() {
		t.Errorf("Bad window detection")
	}

	d.T = d.T.Add(time.Second * 10)
	if !d.IsOdd() {
		t.Errorf("Bad window detection")
	}
}

func TestJson(t *testing.T) {
	d := &Detection{}
	err := d.UnmarshalText([]byte(DATA))
	if err != nil {
		t.Fatal(err)
	}

	msg, err := json.Marshal(*d)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Printf("%q\n", msg)

	d2 := &Detection{}
	err = json.Unmarshal(msg, d2)
	if err != nil {
		t.Fatal(err)
	}

	output, err := d2.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	if string(output) != DATA {
		t.Fatalf("Output mismatch: %q != %q", string(output), DATA)
	}

}
