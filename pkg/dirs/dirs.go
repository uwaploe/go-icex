// Package dirs contains fuctions for working with the output data stream
// from the DIRS (Acoustic Detection) system
package dirs

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	Tick        time.Duration = time.Microsecond * 10
	DoubletLow                = time.Millisecond * 32
	DoubletHigh               = time.Millisecond * 37
)

const (
	F1Q1H1 int = iota
	F1Q1H2
	F1Q1H3
	F1Q1H4
	F1Q2H1
	F1Q2H2
	F1Q2H3
	F1Q2H4
	F2Q1H1
	F2Q1H2
	F2Q1H3
	F2Q1H4
	F2Q2H1
	F2Q2H2
	F2Q2H3
	F2Q2H4
)

// Acoustic detection record. All of the detections are in units
// of 10usec ticks relative to the timestamp.
type Detection struct {
	// Timestamp of the start of the second
	T time.Time `json:"t"`
	// Timestamps of the detections
	Dt [16]time.Time `json:"dt"`
}

// NewDetection creates a new Detection struct from a timestamp and a
// slice of relative detection times.
func NewDetection(t time.Time, dt []int) *Detection {
	d := Detection{T: t}
	for i, value := range dt {
		if value >= 0 {
			d.Dt[i] = d.T.Add(Tick * time.Duration(value))
		}
	}
	return &d
}

// Time returns the detection time for a frequency, quad, and hydrophone triplet.
func (d *Detection) Time(fn, qn, hn int) (t time.Time) {
	i := (hn - 1) + (qn-1)*4 + (fn-1)*8
	if i >= 0 && i < 16 {
		t = d.Dt[i]
	}
	return
}

// Ticks returns the relative detection time for a frequency, quad, and hydrophone triplet.
func (d *Detection) Ticks(fn, qn, hn int) int {
	val := d.Time(fn, qn, hn)
	if val.IsZero() {
		return -1
	} else {
		return int(val.Sub(d.T) / Tick)
	}
}

// IsOdd is used to differentiate between the odd and even platform detections
func (d *Detection) IsOdd() bool {
	window := d.T.Truncate(10*time.Second).Second() / 10
	return (window & 1) == 1
}

// F1q1 returns the relative detection times in ticks for frequency 1 and
// quad 1
func (d *Detection) F1q1() []int {
	dt := make([]int, 4)
	for i := 0; i < 4; i++ {
		if d.Dt[i].IsZero() {
			dt[i] = -1
		} else {
			dt[i] = int(d.Dt[i].Sub(d.T) / Tick)
		}
	}
	return dt
}

// F1q1 returns the relative detection times in ticks for frequency 1 and
// quad 2
func (d *Detection) F1q2() []int {
	dt := make([]int, 4)
	for i := 0; i < 4; i++ {
		if d.Dt[i+4].IsZero() {
			dt[i] = -1
		} else {
			dt[i] = int(d.Dt[i+4].Sub(d.T) / Tick)
		}
	}
	return dt
}

// F1q1 returns the relative detection times in ticks for frequency 2 and
// quad 1
func (d *Detection) F2q1() []int {
	dt := make([]int, 4)
	for i := 0; i < 4; i++ {
		if d.Dt[i+8].IsZero() {
			dt[i] = -1
		} else {
			dt[i] = int(d.Dt[i+8].Sub(d.T) / Tick)
		}
	}
	return dt
}

// F1q1 returns the relative detection times in ticks for frequency 2 and
// quad 2
func (d *Detection) F2q2() []int {
	dt := make([]int, 4)
	for i := 0; i < 4; i++ {
		if d.Dt[i+12].IsZero() {
			dt[i] = -1
		} else {
			dt[i] = int(d.Dt[i+12].Sub(d.T) / Tick)
		}
	}
	return dt
}

// Data time-stamp format: dd/mm/yyyy HH:MM:SS
const time_format = "02/01/2006 15:04:05"

// MarshalText implements the encoding.TextMarshaler interface.
func (d *Detection) MarshalText() ([]byte, error) {
	cols := make([]string, 17)
	cols[0] = d.T.Format(time_format)

	base := int(1)
	for i, t := range d.F1q1() {
		cols[base+i] = strconv.FormatInt(int64(t), 10)
	}
	base += 4

	for i, t := range d.F1q2() {
		cols[base+i] = strconv.FormatInt(int64(t), 10)
	}
	base += 4

	for i, t := range d.F2q1() {
		cols[base+i] = strconv.FormatInt(int64(t), 10)
	}
	base += 4

	for i, t := range d.F2q2() {
		cols[base+i] = strconv.FormatInt(int64(t), 10)
	}

	return []byte(strings.Join(cols, "\t")), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (d *Detection) UnmarshalText(text []byte) error {
	var err error

	fields := strings.Split(string(text), "\t")
	if len(fields) != (len(d.Dt) + 1) {
		return errors.New("Invalid record length")
	}

	t, err := time.Parse(time_format, fields[0])
	if err != nil {
		return errors.Wrap(err, "parsing timestamp")
	}
	d.T = t.UTC()

	for i, f := range fields[1:] {
		val, err := strconv.ParseInt(f, 10, 32)
		if err != nil {
			return errors.Wrapf(err, "parsing field %d", i+1)
		}
		if val >= 0 {
			d.Dt[i] = d.T.Add(time.Duration(val) * Tick)
		} else {
			d.Dt[i] = time.Time{}
		}
	}

	return nil
}

// MarshalJSON implements the json.Marshal interface.
func (d Detection) MarshalJSON() ([]byte, error) {
	out := make(map[string]interface{})
	out["t"] = d.T.Unix()
	dts := make([]int, 16)
	for i, val := range d.Dt {
		if val.IsZero() {
			dts[i] = -1
		} else {
			dts[i] = int(val.Sub(d.T) / Tick)
		}
	}
	out["dt"] = dts
	return json.Marshal(out)
}

// UnmarshalJSON implements the json.Unmarshal interface.
func (d *Detection) UnmarshalJSON(data []byte) error {
	var f interface{}
	err := json.Unmarshal(data, &f)
	if err != nil {
		return err
	}

	m, ok := f.(map[string]interface{})
	if !ok {
		return errors.New(fmt.Sprintf("invalid format: %T", f))
	}
	d.T = time.Unix(int64(m["t"].(float64)), 0).UTC()

	dts, ok := m["dt"].([]interface{})
	if !ok {
		return errors.New(fmt.Sprintf("invalid type for Dt: %T", m["dt"]))
	}

	for i, val := range dts {
		dt := val.(float64)
		if dt >= 0 {
			d.Dt[i] = d.T.Add(time.Duration(dt) * Tick)
		} else {
			d.Dt[i] = time.Time{}
		}
	}

	return nil
}
