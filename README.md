# ICEX Applications

This repository contains various applications for the ICEX project. Most
of the applications are for moving data between the "legacy" system
(DIRS+RADS) and the new system (ATRS2).

## Data Flow Diagram

Because the ATRS2 and Seascape systems will be on a classified network,
one-way RS232 serial links are used to bring in data from the unclassified
DIRS and GICS systems.

![Dataflow](dataflow.svg "data-flow diagram")
