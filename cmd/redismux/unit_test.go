package main

import (
	"bytes"
	"testing"

	"github.com/garyburd/redigo/redis"
)

var testdata = struct {
	msg  redis.Message
	resp []byte
}{
	msg:  redis.Message{Channel: "test", Data: []byte("hello world")},
	resp: []byte("*2\r\n$4\r\ntest\r\n$11\r\nhello world\r\n"),
}

func TestRESPFormat(t *testing.T) {
	var buf bytes.Buffer

	err := writeMessage(&buf, testdata.msg)
	if err != nil {
		t.Fatal(err)
	}

	if buf.String() != string(testdata.resp) {
		t.Errorf("%q != %q", buf.String(), testdata.resp)
	}
}
