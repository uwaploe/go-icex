// Redismux multiplexes data from one or more pub-sub channels
// over a serial link.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/tarm/serial"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: redismux [options] device chan [chan ...]

Subscribe to one or more Redis pub-sub channels and write the received
messages to a serial line in the form of a two-element RESP array.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug  = flag.Bool("debug", false, "Enable debugging output")
	server = flag.String("address", "localhost:6379", "Redis server address")
	baud   = flag.Int("baud", 115200, "Serial line baud rate")
)

func reader(psc redis.PubSubConn) <-chan redis.Message {
	c := make(chan redis.Message, 1)

	go func() {
		defer close(c)
		log.Println("Waiting for messages")
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				c <- msg
			}
		}
	}()

	return c
}

func writeMessage(w io.Writer, msg redis.Message) error {
	_, err := w.Write([]byte(fmt.Sprintf("*2\r\n$%d\r\n%s\r\n$%d\r\n",
		len(msg.Channel),
		msg.Channel,
		len(msg.Data))))
	if err != nil {
		return err
	}
	w.Write(msg.Data)
	_, err = w.Write([]byte("\r\n"))
	return err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	cfg := &serial.Config{
		Name:        args[0],
		Baud:        *baud,
		ReadTimeout: time.Second * 3,
	}
	port, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatalf("Cannot open serial port: %v", err)
	}
	wbuf := bufio.NewWriter(port)

	conn, err := redis.Dial("tcp", *server)
	if err != nil {
		log.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: conn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	c := reader(psc)
	for _, arg := range args[1:] {
		psc.Subscribe(arg)
	}

	for msg := range c {
		err = writeMessage(wbuf, msg)
		if err != nil {
			log.Printf("[%s] write message failed: %v", msg.Channel, err)
		}
		wbuf.Flush()
	}
}
