// Redisdemux reads multiplexed pub-sub messages over a serial link
// and publishes them to the local Redis server.
package main

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/tarm/serial"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: redisdemux [options] device

Read multiplexed Redis pub-sub messages over a serial link in RESP
array format and publish them to a Redis server.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug   = flag.Bool("debug", false, "Enable debugging output")
	server  = flag.String("address", "localhost:6379", "Redis server address")
	baud    = flag.Int("baud", 115200, "Serial line baud rate")
	timeout = flag.Duration("timeout", time.Second*60,
		"Serial line timeout")
)

// Read a line and trim the trailing CR-LF
func readLine(rdr *bufio.Reader) ([]byte, error) {
	line, err := rdr.ReadBytes('\n')
	if err != nil {
		return nil, err
	}
	return bytes.TrimRight(line, "\r\n"), nil
}

// Read a length record (decimal integer)
func parseLen(p []byte) (int, error) {
	var n int

	for _, b := range p {
		n *= 10
		if b < '0' || b > '9' {
			return -1, errors.New("illegal bytes in length")
		}
		n += int(b - '0')
	}
	return n, nil
}

// Read the RESP array messages from the serial line. The array has two
// elements, both are "bulk strings". This code was adapted from the
// excellent Redis client package: github.com/garyburd/redigo/redis
func readRESP(rdr *bufio.Reader) (interface{}, error) {
	line, err := readLine(rdr)
	if err != nil {
		return nil, err
	}

	if len(line) == 0 {
		return nil, errors.New("short read")
	}

	switch line[0] {
	case '$':
		n, err := parseLen(line[1:])
		if n < 0 || err != nil {
			return nil, err
		}
		p := make([]byte, n)
		_, err = io.ReadFull(rdr, p)
		if err != nil {
			return nil, err
		}
		if _, err = readLine(rdr); err != nil {
			return nil, err
		}
		return p, nil
	case '*':
		n, err := parseLen(line[1:])
		if n < 0 || err != nil {
			return nil, err
		}
		r := make([]interface{}, n)
		for i := range r {
			r[i], err = readRESP(rdr)
			if err != nil {
				return nil, err
			}
		}
		return r, nil
	}

	return nil, nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	cfg := &serial.Config{
		Name:        args[0],
		Baud:        *baud,
		ReadTimeout: *timeout,
	}
	port, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatalf("Cannot open serial port: %v", err)
	}
	rbuf := bufio.NewReader(port)

	conn, err := redis.Dial("tcp", *server)
	if err != nil {
		log.Fatal(err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal %v", s)
			cancel()
		}
	}()

loop:
	for {
		rec, err := readRESP(rbuf)
		if err != nil {
			log.Println(err)
		} else {
			if val, ok := rec.([]interface{}); ok {
				channel := string(val[0].([]byte))
				conn.Do("PUBLISH", channel, val[1])
				if *debug {
					log.Printf("%v", rec)
				}
			}
		}

		select {
		case <-ctx.Done():
			break loop
		default:
		}
	}
}
