package main

import (
	"bufio"
	"bytes"
	"testing"

	"github.com/garyburd/redigo/redis"
)

var testdata = []struct {
	msg  redis.Message
	resp []byte
}{
	{
		msg:  redis.Message{Channel: "test", Data: []byte("hello world")},
		resp: []byte("*2\r\n$4\r\ntest\r\n$11\r\nhello world\r\n"),
	},
	{
		msg:  redis.Message{Channel: "test", Data: []byte("hello world")},
		resp: []byte("$5\r\nnoise\r\n*2\r\n$4\r\ntest\r\n$11\r\nhello world\r\n"),
	},
}

func TestRESPRead(t *testing.T) {
	buf := bytes.NewBuffer(testdata[0].resp)

	rec, err := readRESP(bufio.NewReader(buf))
	if err != nil {
		t.Error(err)
	}

	val, ok := rec.([]interface{})

	if !ok {
		t.Errorf("Bad return type: %T", rec)
	}

	if string(val[0].([]byte)) != testdata[0].msg.Channel {
		t.Errorf("Wrong channel name: %v", val[0])
	}

	if string(val[1].([]byte)) != string(testdata[0].msg.Data) {
		t.Errorf("Wrong data value: %v", val[1])
	}
}

func TestRESPReadUnsync(t *testing.T) {
	buf := bytes.NewBuffer(testdata[1].resp)

	rdr := bufio.NewReader(buf)
	rec, err := readRESP(rdr)
	if err != nil {
		t.Error(err)
	}

	// The first read will return the wrong type
	val, ok := rec.([]interface{})
	if ok {
		t.Errorf("Bad return type: %T", rec)
	}

	// This one should work
	rec, err = readRESP(rdr)
	if err != nil {
		t.Error(err)
	}

	val, ok = rec.([]interface{})

	if !ok {
		t.Errorf("Bad return type: %T", rec)
	}

	if string(val[0].([]byte)) != testdata[1].msg.Channel {
		t.Errorf("Wrong channel name: %v", val[0])
	}

	if string(val[1].([]byte)) != string(testdata[1].msg.Data) {
		t.Errorf("Wrong data value: %v", val[1])
	}
}
