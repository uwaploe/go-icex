package main

import (
	"context"
	"os"
	"path/filepath"
	"testing"
)

func helperOpenFile(t *testing.T, name string) *os.File {
	path := filepath.Join("testdata", name)
	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	return f
}

func TestReader(t *testing.T) {
	f := helperOpenFile(t, "dirs.out")
	ch := streamData(context.Background(), f)

	i := int(0)
	for _ = range ch {
		i += 1
	}

	if i != 9 {
		t.Errorf("Bad record count: %d != 9", i)
	}
}
