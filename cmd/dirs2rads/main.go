// Dirs2rads reads DIRS data records from a Redis pub-sub channel and
// writes them to a serial port in a format suitable for RADS
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/go-icex/pkg/dirs"
	"github.com/garyburd/redigo/redis"
	"github.com/tarm/serial"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: dirs2rads [options] serialdev

Read DIRS data records from a Redis pub-sub channel and writes them to a
serial port in a format suitable for RADS
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	server = flag.String("server", "localhost:6379",
		"Redis server HOST:PORT")
	channel = flag.String("channel", "data.dirs",
		"Redis pub-sub channel to monitor")
	baud = flag.Int("baud", 115200, "Serial line baud rate")
)

func dirsReader(psc redis.PubSubConn) <-chan *dirs.Detection {
	ch := make(chan *dirs.Detection, 1)
	go func() {
		defer close(ch)
		log.Println("Waiting for messages")
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				rec := dirs.Detection{}
				err := json.Unmarshal(msg.Data, &rec)
				if err != nil {
					log.Printf("JSON decode error (data.dirs): %v", err)
				}
				ch <- &rec
			}

		}
	}()

	return ch
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var wbuf *bufio.Writer
	if strings.HasPrefix(args[0], "/dev/tty") {
		cfg := &serial.Config{
			Name: args[0],
			Baud: *baud,
		}
		port, err := serial.OpenPort(cfg)
		if err != nil {
			log.Fatalf("Cannot open serial port: %v", err)
		}
		wbuf = bufio.NewWriter(port)
	} else {
		f, err := os.Create(args[0])
		if err != nil {
			log.Fatalf("Cannot open output file: %v", err)
		}
		wbuf = bufio.NewWriter(f)
	}
	defer wbuf.Flush()

	conn, err := redis.Dial("tcp", *server)
	if err != nil {
		log.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: conn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	var last_t time.Time

	ch := dirsReader(psc)
	psc.Subscribe(*channel)
	for rec := range ch {
		if !last_t.IsZero() && rec.T.After(last_t) {
			_, err = wbuf.Write([]byte("X\r\n"))
			if err != nil {
				log.Printf("Write error: %+v", err)
			}
		}
		text, err := rec.MarshalText()
		if err != nil {
			log.Printf("Write error: %+v", err)
		} else {
			wbuf.Write(text)
			_, err = wbuf.Write([]byte("\r\n"))
			if err != nil {
				log.Printf("Write error: %+v", err)
			}
		}
		last_t = rec.T
		wbuf.Flush()
	}
}
