// Atrs2sim read DIRS detection records from a Redis pub-sub channel and
// use them to create "reduced" Seascape data records which are written to
// standard output. It's primary use is to simulate the output of the new
// ATRS2 detection program.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/go-icex/pkg/dirs"
	"bitbucket.org/uwaploe/go-icex/pkg/seascape"
	"github.com/garyburd/redigo/redis"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: atrs2sim [options]

Read DIRS data records from a Redis pub-sub channel and write them to
standard output in the format expected by atrs2ctl.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	address = flag.String("rd-address", "localhost:6379",
		"Redis server HOST:PORT")
)

const Snr int = 20
const PulseWidth int = 100

func buildTimerRecord(d *dirs.Detection, hn int, framing bool) *seascape.TimerRecord {
	tr := seascape.TimerRecord{
		Bands: [4]seascape.Detection{
			seascape.NoSignal,
			seascape.NoSignal,
			seascape.NoSignal,
			seascape.NoSignal,
		},
		Framing: seascape.NoSignal,
	}

	tr.Id = uint(hn)
	tr.T = d.T
	var band int = 0
	if d.IsOdd() {
		band = 1
	}

	// Platform frequency band
	tr.Bands[band].Dt = d.Ticks(1, 1, hn)
	if tr.Bands[band].Dt >= 0 {
		tr.Bands[band].Snr = Snr
		tr.Bands[band].Pw = PulseWidth
	}

	// Weapon frequency band
	tr.Bands[3].Dt = d.Ticks(2, 1, hn)
	if tr.Bands[3].Dt >= 0 {
		tr.Bands[3].Snr = Snr
		tr.Bands[3].Pw = PulseWidth
	}

	// If a framing pulse was detected, copy the weapon detection time
	// into the Framing band.
	if framing {
		tr.Framing.Dt = tr.Bands[3].Dt
		tr.Framing.Snr = Snr
		tr.Framing.Pw = PulseWidth
	}

	return &tr
}

func buildTimerPacket(w io.Writer, d *dirs.Detection, framing []bool) {
	for hn := 1; hn <= 4; hn++ {
		tr := buildTimerRecord(d, hn, framing[hn-1])
		buf, err := tr.MarshalReduced()
		if err != nil {
			log.Print(err)
			continue
		}
		w.Write(buf)
		w.Write([]byte("\n"))
	}
}

// Check for the torpedo frame pulses, two pulses sent 35 ms apart every
// ten seconds.
func frameDetections(d0, d1 *dirs.Detection) []bool {
	d := make([]time.Duration, 16)
	state := make([]bool, 4)
	for i := 0; i < 16; i++ {
		if !(d0.Dt[i].IsZero() || d1.Dt[i].IsZero()) {
			d[i] = d1.Dt[i].Sub(d0.Dt[i])
		}
	}

	// Torpedo detections are on Frequency-2/Quad-1
	j := int(0)
	for i := dirs.F2Q1H1; i <= dirs.F2Q1H4; i++ {
		state[j] = d[i] > dirs.DoubletLow && d[i] < dirs.DoubletHigh
		j++
	}

	return state
}

func dirsReader(psc redis.PubSubConn) <-chan struct{} {
	c := make(chan struct{}, 1)
	go func() {
		defer close(c)
		var (
			rec_last, rec dirs.Detection
			t_last        time.Time
			err           error
		)

		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				switch msg.Channel {
				case "data.dirs":
					err = json.Unmarshal(msg.Data, &rec)
					if err != nil {
						log.Printf("JSON decode error (data.dirs): %v", err)
					}

					if rec_last.T.IsZero() {
						// Add a one sample delay so we can detect framing pulses
						rec_last = rec
					} else {
						framing := frameDetections(&rec_last, &rec)
						if rec_last.T.After(t_last) {
							t_last = rec_last.T
							buildTimerPacket(os.Stdout, &rec_last, framing)
						}
						rec_last = rec
					}
				}
			}

		}
	}()

	return c
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	psconn, err := redis.Dial("tcp", *address)
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	c := dirsReader(psc)
	psc.Subscribe("data.dirs")

	<-c
}
