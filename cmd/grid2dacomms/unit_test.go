package main

import (
	"testing"

	"bitbucket.org/uwaploe/gics/api"
)

var testdata = struct {
	input  api.Grid
	expect []byte
}{
	input: api.Grid{
		Tsec: 1450372480,
		Origin: &api.Point{
			Latitude:  476552000,
			Longitude: -1223168834,
		},
		Azimuth: 42000,
	},
	expect: []byte("2015-12-17 17:14:40 47.655200 -122.316883 42.0\r\n"),
}

func TestFormat(t *testing.T) {
	output := formatGrid(&(testdata.input))
	if string(output) != string(testdata.expect) {
		t.Errorf("Bad format: %q != %q", output, testdata.expect)
	}
}
