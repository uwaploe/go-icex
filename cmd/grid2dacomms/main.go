// Grid2dacomms subscribes to the Redis pub-sub channel containing the ICEX
// grid parameters (origin and azimuth) and sends the information over a
// serial line to the Digital Acoustic Communications system.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"github.com/tarm/serial"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: grid2dacomms [options] serialdev

Subscribes to the Redis pub-sub channel containing the ICEX grid
parameters (origin and azimuth) and sends the information over a serial
line to the Digital Acoustic Communications system.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	server = flag.String("server", "localhost:6379",
		"Redis server HOST:PORT")
	channel = flag.String("channel", "data.grid",
		"Redis pub-sub channel to monitor")
	baud = flag.Int("baud", 115200, "Serial line baud rate")
)

func gridReader(psc redis.PubSubConn) <-chan *api.Grid {
	ch := make(chan *api.Grid, 1)
	go func() {
		defer close(ch)
		log.Println("Waiting for messages")
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				rec := api.Grid{}
				err := jsonpb.UnmarshalString(string(msg.Data), &rec)
				if err != nil {
					log.Printf("JSON decode error (%q): %v", msg.Channel, err)
				}
				ch <- &rec
			}
		}
	}()

	return ch
}

func formatGrid(grid *api.Grid) []byte {
	t := time.Unix(grid.Tsec, 0).UTC()
	s := fmt.Sprintf("%s %.6f %.6f %.1f\r\n",
		t.Format("2006-01-02 15:04:05"),
		float64(grid.Origin.Latitude)/api.GEOSCALE,
		float64(grid.Origin.Longitude)/api.GEOSCALE,
		float64(grid.Azimuth)/api.ANGLESCALE)
	return []byte(s)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var wbuf *bufio.Writer
	if strings.HasPrefix(args[0], "/dev/tty") {
		cfg := &serial.Config{
			Name: args[0],
			Baud: *baud,
		}
		port, err := serial.OpenPort(cfg)
		if err != nil {
			log.Fatalf("Cannot open serial port: %v", err)
		}
		wbuf = bufio.NewWriter(port)
	} else {
		f, err := os.Create(args[0])
		if err != nil {
			log.Fatalf("Cannot open output file: %v", err)
		}
		wbuf = bufio.NewWriter(f)
	}
	defer wbuf.Flush()

	conn, err := redis.Dial("tcp", *server)
	if err != nil {
		log.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: conn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	ch := gridReader(psc)
	psc.Subscribe(*channel)
	for rec := range ch {
		wbuf.Write(formatGrid(rec))
		if err = wbuf.Flush(); err != nil {
			log.Printf("Write error; %v", err)
		}
	}
}
