// Atrs2ss reads ATRS2 detection records from a Redis pub-sub channel and
// creates input packets for Seascape.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/go-icex/pkg/seascape"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: atrs2ss [options] host:port [initfile]

Read ATRS2 data records from a Redis pub-sub channel and send them to
SeaScape running on the specified host and (UDP) port. INITFILE is a
JSON file with initial values for the hydrophone reference points and
the grid parameters.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	address = flag.String("rd-address", "localhost:6379",
		"Redis server HOST:PORT")
)

type State struct {
	api.PointSet `json:"refs"`
	api.Grid     `json:"grid"`
}

var NodeNames = map[uint]string{
	1: "h1",
	2: "h2",
	3: "h3",
	4: "h4",
}

// Read messages from the GPS pub-sub channels and write to a Go channel
func redisReader(psc redis.PubSubConn) <-chan interface{} {
	c := make(chan interface{}, 1)

	go func() {
		defer close(c)

		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				switch msg.Channel {
				case "data.grid":
					grid := api.Grid{}
					err := jsonpb.UnmarshalString(string(msg.Data), &grid)
					if err != nil {
						log.Printf("JSON decode error (data.grid): %v", err)
					} else {
						c <- &grid
					}
				case "data.refs":
					refs := api.PointSet{}
					err := jsonpb.UnmarshalString(string(msg.Data), &refs)
					if err != nil {
						log.Printf("JSON decode error (data.refs): %v", err)
					} else {
						c <- &refs
					}
				case "data.atrs2":
					rec := seascape.TimerRecord{}
					err := json.Unmarshal(msg.Data, &rec)
					if err != nil {
						log.Printf("JSON decode error (data.atrs2): %v", err)
					} else {
						c <- &rec
					}
				}
			}

		}
	}()

	return c
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	conn, err := net.Dial("udp", args[0])
	if err != nil {
		log.Fatal(err)
	}

	var (
		state State
		grid  *api.Grid
		refs  *api.PointSet
	)

	if len(args) > 1 {
		// Load the initial values for the grid and reference points.
		b, err := ioutil.ReadFile(args[1])
		if err != nil {
			log.Fatal(err)
		}

		if err = json.Unmarshal(b, &state); err != nil {
			log.Fatal(err)
		}
		grid = &state.Grid
		refs = &state.PointSet
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	psconn, err := redis.Dial("tcp", *address)
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	ch := redisReader(psc)
	channels := []interface{}{"data.grid", "data.refs", "data.atrs2"}
	psc.Subscribe(channels...)

	tp := seascape.NewPacket()
	for rec := range ch {
		switch v := rec.(type) {
		case *api.Grid:
			grid = v
		case *api.PointSet:
			refs = v
		case *seascape.TimerRecord:
			if grid != nil && refs != nil {
				if name := NodeNames[v.Id]; name != "" {
					v.Lat = float64(refs.Points[name].Latitude) / api.GEOSCALE
					v.Lon = float64(refs.Points[name].Longitude) / api.GEOSCALE
					v.Fix = "A"
					if !tp.AddRecord(v) {
						// Packet is full. Add grid data and send
						tp.AddGrid(float64(grid.Origin.Latitude)/api.GEOSCALE,
							float64(grid.Origin.Longitude)/api.GEOSCALE,
							float64(grid.Azimuth)/api.ANGLESCALE)
						if frame, err := tp.MarshalText(); err == nil {
							conn.Write(frame)
							if *debug {
								log.Printf("%q\n", frame)
							}
						} else {
							log.Printf("Packet send failed: %v", err)
						}
						// Clear the packet
						tp.Reset()
						tp.AddRecord(v)
					}
				} else {
					log.Printf("Location unknown for Node %d", v.Id)
				}
			} else {
				log.Println("No GPS data available!")
			}
		}
	}
}
