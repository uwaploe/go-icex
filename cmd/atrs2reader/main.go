// Atrs2reader reads the detection data records from ATRS2 via a serial
// port and publishes them on a Redis channel.
package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"bitbucket.org/uwaploe/go-icex/pkg/seascape"
	"github.com/garyburd/redigo/redis"
	"github.com/tarm/serial"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: atrs2reader [options] device

Read ATRS2 output from a serial device and publish the data records
on a Redis pub-sub channel.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug   = flag.Bool("debug", false, "Enable debugging output")
	server  = flag.String("address", "localhost:6379", "Redis server address")
	channel = flag.String("channel", "data.atrs2",
		"Redis pub-sub channel to publish data")
	baud    = flag.Int("baud", 115200, "Serial line baud rate")
	timeout = flag.Duration("timeout", 0, "Input data timeout")
)

// Read Detection records from an io.Reader and write to a channel
func streamData(ctx context.Context, rdr io.Reader) <-chan *seascape.TimerRecord {
	c := make(chan *seascape.TimerRecord, 1)
	go func() {
		defer close(c)

		log.Println("Waiting for data")
		scanner := bufio.NewScanner(rdr)
		for scanner.Scan() {
			d := &seascape.TimerRecord{}
			if b := scanner.Bytes(); len(b) > 1 {
				if err := d.UnmarshalReduced(b); err == nil {
					select {
					case <-ctx.Done():
						return
					default:
					}
					c <- d
					if *debug {
						log.Print(*d)
					}
				} else {
					log.Printf("Cannot parse data record: %v", err)
				}
			}
		}

		if err := scanner.Err(); err != nil {
			log.Printf("Error reading ATRS2 output: %v", err)
		}

	}()

	return c
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	cfg := &serial.Config{
		Name:        args[0],
		Baud:        *baud,
		ReadTimeout: *timeout,
	}
	port, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatalf("Cannot open serial port: %v", err)
	}

	conn, err := redis.Dial("tcp", *server)
	if err != nil {
		log.Fatal(err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal %v", s)
			cancel()
		}
	}()

	c := streamData(ctx, port)
	for detection := range c {
		msg, err := json.Marshal(*detection)
		if err != nil {
			log.Printf("Encoding error: %v", err)
		} else {
			conn.Do("PUBLISH", *channel, msg)
		}
	}
}
