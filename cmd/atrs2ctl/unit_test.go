package main

import (
	"testing"

	"github.com/BurntSushi/toml"
)

func TestConfigParse(t *testing.T) {
	var cfg sysConfig

	if err := toml.Unmarshal([]byte(DEFAULT), &cfg); err != nil {
		t.Fatal(err)
	}

	if cfg.Redis.Address != "localhost:6379" {
		t.Errorf("Redis address mismatch: %q", cfg.Redis.Address)
	}

	if len(cfg.Redis.Chans) != 2 {
		t.Errorf("Bad Redis channel count: %d", len(cfg.Redis.Chans))
	}

	if cfg.Prog.Args != nil {
		t.Errorf("Unexpected value for Prog.Args: %v (%T)",
			cfg.Prog.Args, cfg.Prog.Args)
	}
}
