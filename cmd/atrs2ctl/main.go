// Atrs2ctl runs the ATRS2 detection program, combines its output with the
// latest GPS information and sends the combined data to Seascape.
package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"syscall"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/go-icex/pkg/seascape"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"github.com/imdario/mergo"
)

type redisConfig struct {
	Address string            `toml:"address"`
	Chans   map[string]string `toml:"chans,omitempty"`
}

type seascapeConfig struct {
	Address string `toml:"address"`
}

type progConfig struct {
	Path string   `toml:"path"`
	Args []string `toml:"args,omitempty"`
	Dir  string   `toml:"dir,omitempty"`
}

type sysConfig struct {
	Redis    redisConfig    `toml:"redis"`
	Seascape seascapeConfig `toml:"seascape"`
	Prog     progConfig     `toml:"prog"`
}

type State struct {
	api.PointSet `json:"refs"`
	api.Grid     `json:"grid"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: atrs2ctl [options] [gpsfile]

Run the ATRS2 detection program, combine its output with the latest GPS
information and send the combined data to Seascape. Gpsfile is a JSON
file containing the initial GPS location of each hydrophone along with
the grid origin and azimuth.
`

var DEFAULT = `
[redis]
address = "localhost:6379"
[redis.chans]
gps = "data.refs"
grid = "data.grid"
[seascape]
address = "10.0.0.10:4001"
[prog]
path = "/usr/local/bin/atrs2"
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	config = flag.String("config", "", "Configuration file")
)

var NodeNames = map[uint]string{
	1: "h1",
	2: "h2",
	3: "h3",
	4: "h4",
}

// Parse detection records from the ATRS2 program and write them
// to a channel.
func detectionReader(rdr io.Reader) <-chan *seascape.TimerRecord {
	// The program will produce four records for every detection
	c := make(chan *seascape.TimerRecord, 4)

	go func() {
		defer close(c)
		scanner := bufio.NewScanner(rdr)
		for scanner.Scan() {
			rec := &seascape.TimerRecord{}
			b := scanner.Bytes()
			if len(b) > 1 {
				err := rec.UnmarshalReduced(b)
				if err != nil {
					log.Printf("Detection read error: %v", err)
					continue
				}
				c <- rec
			}
		}

		if err := scanner.Err(); err != nil {
			log.Printf("Detection read error: %v", err)
		}
	}()

	return c
}

// Read messages from the GPS pub-sub channels and write to a Go channel
func redisReader(psc redis.PubSubConn, gridchan, gpschan string) <-chan interface{} {
	c := make(chan interface{}, 1)

	go func() {
		defer close(c)

		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				switch msg.Channel {
				case gridchan:
					grid := api.Grid{}
					err := jsonpb.UnmarshalString(string(msg.Data), &grid)
					if err != nil {
						log.Printf("JSON decode error (%q): %v", gridchan, err)
					}
					c <- &grid
				case gpschan:
					refs := api.PointSet{}
					err := jsonpb.UnmarshalString(string(msg.Data), &refs)
					if err != nil {
						log.Printf("JSON decode error (%q): %v", gpschan, err)
					}
					c <- &refs
				}
			}

		}
	}()

	return c
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Fprintf(os.Stdout, "%s", DEFAULT)
		os.Exit(0)
	}

	var (
		err         error
		defcfg, cfg sysConfig
	)

	if err = toml.Unmarshal([]byte(DEFAULT), &defcfg); err != nil {
		log.Fatalf("Cannot parse default config: %v", err)
	}

	if *config != "" {
		b, err := ioutil.ReadFile(*config)
		if err != nil {
			log.Fatalf("Cannot read config file: %v", err)
		}

		if err = toml.Unmarshal(b, &cfg); err != nil {
			log.Fatalf("Cannot parse default config: %v", err)
		}
	}

	mergo.Merge(&cfg, defcfg)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Initialize UDP "connection" to Seascape
	conn, err := net.Dial("udp", cfg.Seascape.Address)
	if err != nil {
		log.Fatal(err)
	}

	// Initialize Redis pub-sub connection
	psconn, err := redis.Dial("tcp", cfg.Redis.Address)
	if err != nil {
		log.Fatalf("Cannot access Redis: %v", err)
	}
	defer psconn.Close()
	psc := redis.PubSubConn{Conn: psconn}

	// Top level context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var (
		state State
		grid  *api.Grid
		refs  *api.PointSet
	)

	args := flag.Args()
	if len(args) > 0 {
		// Load the initial values for the grid and reference points.
		b, err := ioutil.ReadFile(args[0])
		if err != nil {
			log.Fatal(err)
		}

		if err = json.Unmarshal(b, &state); err != nil {
			log.Fatal(err)
		}
		grid = &state.Grid
		refs = &state.PointSet
	}

	ch_redis := redisReader(psc, cfg.Redis.Chans["grid"], cfg.Redis.Chans["gps"])
	psc.Subscribe(cfg.Redis.Chans["grid"], cfg.Redis.Chans["gps"])

	// Prepare to run the detection program as a child process
	cmd := exec.CommandContext(ctx, cfg.Prog.Path, cfg.Prog.Args...)
	cmd.Dir = cfg.Prog.Dir
	rdr, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}
	ch_prog := detectionReader(rdr)

	tp := seascape.NewPacket()
	err = cmd.Start()
	if err != nil {
		log.Fatalf("Cannot start %q: %v", cfg.Prog.Path, err)
	}

loop:
	for {
		select {
		case tr, more := <-ch_prog:
			if !more {
				log.Println("Detection program exited ...")
				break loop
			}

			if grid != nil && refs != nil {
				if name := NodeNames[tr.Id]; name != "" {
					tr.Lat = float64(refs.Points[name].Latitude) / api.GEOSCALE
					tr.Lon = float64(refs.Points[name].Longitude) / api.GEOSCALE
					tr.Fix = "A"
					if !tp.AddRecord(tr) {
						// Packet is full. Add grid data and send
						tp.AddGrid(float64(grid.Origin.Latitude)/api.GEOSCALE,
							float64(grid.Origin.Longitude)/api.GEOSCALE,
							float64(grid.Azimuth)/api.ANGLESCALE)
						if frame, err := tp.MarshalText(); err == nil {
							conn.Write(frame)
							if *debug {
								log.Printf("%q\n", frame)
							}
						} else {
							log.Printf("Packet send failed: %v", err)
						}
						// Clear the packet
						tp.Reset()
						tp.AddRecord(tr)
					}
				} else {
					log.Printf("Location unknown for Node %d", tr.Id)
				}
			} else {
				log.Println("No GPS data available!")
			}
		case msg := <-ch_redis:
			switch v := msg.(type) {
			case *api.Grid:
				grid = v
			case *api.PointSet:
				refs = v
			}
		case s, more := <-sigs:
			if more {
				log.Printf("Got signal: %v", s)
			}
			break loop
		}
	}

	psc.Unsubscribe()
	cancel()
	log.Println("Waiting for child process to exit ...")
	cmd.Wait()
}
