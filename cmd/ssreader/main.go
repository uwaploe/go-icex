// Ssreader implements a TCP server to receive custom fix messages from
// Seascape. Each message contains the name and lat-lon of a tracked target.
// The positions are converted to grid positions and published via Redis.
package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/go-icex/pkg/seascape"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"github.com/imdario/mergo"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

type sysConfig struct {
	Server serviceConfig `toml:"server"`
	Redis  serviceConfig `toml:"redis"`
	Mapper serviceConfig `toml:"mapper"`
}

type serviceConfig struct {
	Address string `toml:"address"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: ssreader [options]

Implements a TCP server to receive custom fix messages from Seascape. Each
message contains the name and lat-lon of a tracked target.  The positions
are converted to grid positions and published via Redis.
`

var DEFAULT = `
[server]
address = ":10130"
[redis]
address = "localhost:6379"
[mapper]
address = "localhost:10000"
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	config = flag.String("config", "", "Configuration file")
)

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func handleConnection(conn net.Conn, m api.MapperClient, pool *redis.Pool) {
	defer conn.Close()

	var (
		p   api.Point
		rec seascape.CustomOutput
	)

	ma := jsonpb.Marshaler{EmitDefaults: true}

	in := bufio.NewReader(conn)
	if line, err := in.ReadBytes('\n'); err == nil {
		if err = rec.UnmarshalText(line); err == nil {
			rconn := pool.Get()
			defer rconn.Close()

			p.Id = rec.Name
			p.Tsec = rec.T.Unix()
			p.Latitude = int32(rec.Latitude * api.GEOSCALE)
			p.Longitude = int32(rec.Longitude * api.GEOSCALE)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
			defer cancel()
			if gp, err := m.ToGrid(ctx, &p); err == nil {
				if msg, err := ma.MarshalToString(gp); err == nil {
					rconn.Do("PUBLISH", "tracks."+gp.Id, msg)
				} else {
					log.Println(err)
				}
			} else {
				log.Println(err)
			}
		} else {
			log.Println(err)
		}
	} else {
		log.Println(err)
	}
}

func loadConfiguration(base, cfgfile string, cfg *sysConfig) error {
	var (
		defcfg sysConfig
		err    error
	)

	if err = toml.Unmarshal([]byte(base), &defcfg); err != nil {
		return errors.Wrap(err, "default config")
	}

	if cfgfile != "" {
		b, err := ioutil.ReadFile(cfgfile)
		if err != nil {
			return errors.Wrap(err, "read config file")
		}

		if err = toml.Unmarshal(b, cfg); err != nil {
			return errors.Wrap(err, "parse config file")
		}
	}

	mergo.Merge(cfg, defcfg)

	return nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Fprintf(os.Stdout, "%s", DEFAULT)
		os.Exit(0)
	}

	var (
		err error
		cfg sysConfig
	)

	if err = loadConfiguration(DEFAULT, *config, &cfg); err != nil {
		log.Fatal(err)
	}

	// Initialize a pool of Redis connections
	pool := newPool(cfg.Redis.Address)

	// Connect to the Mapper gRPC server
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(cfg.Mapper.Address, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := api.NewMapperClient(conn)

	listener, err := net.Listen("tcp", cfg.Server.Address)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Waiting for connections")

	for {
		c, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}

		go handleConnection(c, client, pool)
	}
}
