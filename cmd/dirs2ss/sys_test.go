package main

import (
	"testing"

	"bitbucket.org/uwaploe/go-icex/pkg/dirs"
)

var DATA = "17/12/2015 17:14:50\t407\t1067\t1057\t400\t-1\t-1\t-1\t-1\t6733\t5667\t6080\t3463\t-1\t-1\t-1\t-1"

func TestConv(t *testing.T) {
	d := &dirs.Detection{}
	err := d.UnmarshalText([]byte(DATA))
	if err != nil {
		t.Fatal(err)
	}

	expected := d.F1q1()
	band := int(1)
	for hn := 1; hn <= 4; hn++ {
		tr := buildTimerRecord(d, hn, false)
		if tr.Bands[band].Dt != expected[hn-1] {
			t.Errorf("Dt value mismatch for Node %d: %v != %v",
				tr.Id, tr.Bands[band].Dt, expected[hn-1])
		}
	}

	expected = d.F2q1()
	band = int(3)
	for hn := 1; hn <= 4; hn++ {
		tr := buildTimerRecord(d, hn, false)
		if tr.Bands[band].Dt != expected[hn-1] {
			t.Errorf("Dt value mismatch for Node %d: %v != %v",
				tr.Id, tr.Bands[band].Dt, expected[hn-1])
		}
	}
}

var FRAMING = []string{
	"17/12/2015 17:16:10\t1667\t2600\t2423\t1393\t-1\t-1\t-1\t-1\t1637\t2570\t2370\t1200\t-1\t-1\t-1\t-1",
	"17/12/2015 17:16:10\t-1\t-1\t-1\t-1\t-1\t-1\t-1\t-1\t5153\t8110\t5850\t4693\t-1\t-1\t-1\t-1",
}

func TestFrameDetect(t *testing.T) {
	var d0, d1 dirs.Detection
	err := d0.UnmarshalText([]byte(FRAMING[0]))
	if err != nil {
		t.Fatal(err)
	}

	err = d1.UnmarshalText([]byte(FRAMING[1]))
	if err != nil {
		t.Fatal(err)
	}

	expected := []bool{true, false, true, true}
	state := frameDetections(&d0, &d1)
	for i, s := range state {
		if s != expected[i] {
			t.Errorf("Framing pulse state error on H%d", i+1)
		}
	}
}
