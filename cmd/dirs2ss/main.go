// Package main provides a command-line application to read DIRS detection
// records from a Redis pub-sub channel and create input packets for
// Seascape.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/go-icex/pkg/dirs"
	"bitbucket.org/uwaploe/go-icex/pkg/seascape"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: dirs2ss [options] host:port [initfile]

Read DIRS data records from a Redis pub-sub channel and send them to
SeaScape running on the specified host and (UDP) port. INITFILE is a
JSON file with initial values for the hydrophone reference points and
the grid parameters.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	address = flag.String("rd-address", "localhost:6379",
		"Redis server HOST:PORT")
)

const Snr int = 20
const PulseWidth int = 100

type State struct {
	api.PointSet `json:"refs"`
	api.Grid     `json:"grid"`
}

func buildTimerRecord(d *dirs.Detection, hn int, framing bool) *seascape.TimerRecord {
	tr := seascape.TimerRecord{
		Bands: [4]seascape.Detection{
			seascape.NoSignal,
			seascape.NoSignal,
			seascape.NoSignal,
			seascape.NoSignal,
		},
		Framing: seascape.NoSignal,
	}

	tr.Id = uint(hn)
	tr.T = d.T
	var band int = 0
	if d.IsOdd() {
		band = 1
	}

	// Platform frequency band
	tr.Bands[band].Dt = d.Ticks(1, 1, hn)
	if tr.Bands[band].Dt >= 0 {
		tr.Bands[band].Snr = Snr
		tr.Bands[band].Pw = PulseWidth
	}

	// Weapon frequency band
	tr.Bands[3].Dt = d.Ticks(2, 1, hn)
	if tr.Bands[3].Dt >= 0 {
		tr.Bands[3].Snr = Snr
		tr.Bands[3].Pw = PulseWidth
	}

	// If a framing pulse was detected, copy the weapon detection time
	// into the Framing band.
	if framing {
		tr.Framing.Dt = tr.Bands[3].Dt
		tr.Framing.Snr = Snr
		tr.Framing.Pw = PulseWidth
	}

	return &tr
}

func buildTimerPacket(d *dirs.Detection, refs *api.PointSet,
	grid *api.Grid, framing []bool) *seascape.TimerPacket {
	tp := seascape.NewPacket()

	for hn := 1; hn <= 4; hn++ {
		name := fmt.Sprintf("h%d", hn)
		tr := buildTimerRecord(d, hn, framing[hn-1])
		tr.Lat = float64(refs.Points[name].Latitude) / api.GEOSCALE
		tr.Lon = float64(refs.Points[name].Longitude) / api.GEOSCALE
		tr.Fix = "A"
		tp.AddRecord(tr)
	}

	tp.AddGrid(float64(grid.Origin.Latitude)/api.GEOSCALE,
		float64(grid.Origin.Longitude)/api.GEOSCALE,
		float64(grid.Azimuth)/api.ANGLESCALE)

	return tp
}

// Check for the torpedo frame pulses, two pulses sent 35 ms apart every
// ten seconds.
func frameDetections(d0, d1 *dirs.Detection) []bool {
	d := make([]time.Duration, 16)
	state := make([]bool, 4)
	for i := 0; i < 16; i++ {
		if !(d0.Dt[i].IsZero() || d1.Dt[i].IsZero()) {
			d[i] = d1.Dt[i].Sub(d0.Dt[i])
		}
	}

	// Torpedo detections are on Frequency-2/Quad-1
	j := int(0)
	for i := dirs.F2Q1H1; i <= dirs.F2Q1H4; i++ {
		state[j] = d[i] > dirs.DoubletLow && d[i] < dirs.DoubletHigh
		j++
	}

	return state
}

func dirsReader(psc redis.PubSubConn,
	refs0 *api.PointSet, grid0 *api.Grid) <-chan *seascape.TimerPacket {
	c := make(chan *seascape.TimerPacket, 1)
	go func() {
		defer close(c)
		var (
			refs          api.PointSet
			grid          api.Grid
			refsp         *api.PointSet
			gridp         *api.Grid
			rec_last, rec dirs.Detection
			t_last        time.Time
			err           error
		)

		refsp = refs0
		gridp = grid0

		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				switch msg.Channel {
				case "data.grid":
					err = jsonpb.UnmarshalString(string(msg.Data), &grid)
					if err != nil {
						log.Printf("JSON decode error (data.grid): %v", err)
					} else {
						gridp = &grid
					}
				case "data.refs":
					err = jsonpb.UnmarshalString(string(msg.Data), &refs)
					if err != nil {
						log.Printf("JSON decode error (data.refs): %v", err)
					} else {
						refsp = &refs
					}
				case "data.dirs":
					err = json.Unmarshal(msg.Data, &rec)
					if err != nil {
						log.Printf("JSON decode error (data.dirs): %v", err)
					}

					if rec_last.T.IsZero() {
						// Add a one sample delay so we can detect framing pulses
						rec_last = rec
					} else {
						framing := frameDetections(&rec_last, &rec)
						if rec_last.T.After(t_last) {
							t_last = rec_last.T
							if refsp != nil && gridp != nil {
								c <- buildTimerPacket(&rec_last, refsp, gridp, framing)
							} else {
								log.Printf("No GPS data available")
							}
						}
						rec_last = rec
					}
				}
			}

		}
	}()

	return c
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	conn, err := net.Dial("udp", args[0])
	if err != nil {
		log.Fatal(err)
	}

	var (
		state State
		grid  *api.Grid
		refs  *api.PointSet
	)

	if len(args) > 1 {
		file, err := ioutil.ReadFile(args[1])
		if err != nil {
			log.Fatal(err)
		}

		if err := json.Unmarshal(file, &state); err != nil {
			log.Fatal(err)
		}

		grid = &state.Grid
		refs = &state.PointSet
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	psconn, err := redis.Dial("tcp", *address)
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	c := dirsReader(psc, refs, grid)
	channels := []interface{}{"data.grid", "data.refs", "data.dirs"}
	psc.Subscribe(channels...)

	for rec := range c {
		if frame, err := rec.MarshalText(); err == nil {
			conn.Write(frame)
			if *debug {
				log.Printf("%q\n", frame)
			}
		}
	}
}
