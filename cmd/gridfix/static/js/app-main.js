// Module loader
require.config({
    paths: {
        baseUrl: "/static/js",
        jquery: "lib/jquery.min",
        moment: "lib/moment.min",
        app: "./app"
    },
    config: {
        moment: {
            noGlobal: true
        }
    }
});
