/**
 * Application code
 */
define(["jquery", "moment"],
       function($, M) {

           // Time-stamp display format
           var tformat = "YYYY-MM-DD HH:mm:ss";

           function readStream(data_url) {
               var source = new EventSource(data_url);
               source.addEventListener("tracks.PLATFORM-1", function(e){
                   var data = JSON.parse(e.data);
                   var x = Number(data.x)/1.0e3;
                   var y = Number(data.y)/1.0e3;
                   $("#platform-1 .x-pos").text(x.toFixed(1));
                   $("#platform-1 .y-pos").text(y.toFixed(1));
                   $("#platform-1 .tstamp").text(M(Number(data.tsec)*1000).utc().format(tformat) + "Z");
               });

               source.addEventListener("tracks.PLATFORM-2", function(e) {
                   var data = JSON.parse(e.data);
                   var x = Number(data.x)/1.0e3;
                   var y = Number(data.y)/1.0e3;
                   $("#platform-2 .x-pos").text(x.toFixed(1));
                   $("#platform-2 .y-pos").text(y.toFixed(1));
                   $("#platform-2 .tstamp").text(M(Number(data.tsec)*1000).utc().format(tformat) + "Z");
               });

               source.addEventListener("tracks.PLATFORM-3", function(e) {
                   var data = JSON.parse(e.data);
                   var x = Number(data.x)/1.0e3;
                   var y = Number(data.y)/1.0e3;
                   $("#platform-3 .x-pos").text(x.toFixed(1));
                   $("#platform-3 .y-pos").text(y.toFixed(1));
                   $("#platform-3 .tstamp").text(M(Number(data.tsec)*1000).utc().format(tformat) + "Z");
               });

               source.addEventListener("tracks.TORPEDO-1", function(e) {
                   var data = JSON.parse(e.data);
                   var x = Number(data.x)/1.0e3;
                   var y = Number(data.y)/1.0e3;
                   $("#torpedo-1 .x-pos").text(x.toFixed(1));
                   $("#torpedo-1 .y-pos").text(y.toFixed(1));
                   $("#torpedo-1 .tstamp").text(M(Number(data.tsec)*1000).utc().format(tformat) + "Z");
               });

           }

           return {
               readStream: readStream
           };
       });
