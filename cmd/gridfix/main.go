// Gridfix provides a simple HTTP server to provide the most recent
// grid fixes for one or more targets being tracked by Seascape.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/go-icex/pkg/sse"
	"github.com/garyburd/redigo/redis"
	"github.com/gorilla/mux"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: gridfix [options]

Run a simple HTTP server to display to most recent grid fixes for
all of the targets being tracked by Seascape.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	redis_sv = flag.String("redis", "localhost:6379",
		"Redis server address")
	address = flag.String("address", ":8081", "HTTP server address")
	static  = flag.String("static", "./static", "Top level directory for static files")
)

// Read messages from one or more Redis pub-sub channels and send the
// contents as an sse.Event to an sse.Broker. The function returns when
// unsubscribed from all channels or if an error occurs.
func dataReader(psc redis.PubSubConn, b *sse.Broker) {
	log.Println("Waiting for messages")
	for {
		switch msg := psc.Receive().(type) {
		case error:
			log.Println(msg)
			return
		case redis.Subscription:
			if msg.Count == 0 {
				log.Println("Pubsub channel closed")
				return
			} else {
				log.Printf("Subscribed to %q", msg.Channel)
			}
		case redis.PMessage:
			ev := sse.Event{
				Name: []byte(msg.Channel),
				Data: msg.Data,
			}
			b.Notifier <- ev
			if *debug {
				log.Printf("SEND: %q", ev.String())
			}
		case redis.Message:
			ev := sse.Event{
				Name: []byte(msg.Channel),
				Data: msg.Data,
			}
			b.Notifier <- ev
			if *debug {
				log.Printf("SEND: %q", ev.String())
			}
		}
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	psconn, err := redis.Dial("tcp", *redis_sv)
	if err != nil {
		log.Fatal(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Setup the HTTP mux
	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.Write([]byte(HOMEPAGE))
	})
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/",
		http.FileServer(http.Dir(*static))))
	b := sse.NewBroker()
	r.Handle("/stream", b)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	svc := &http.Server{
		Handler:     r,
		Addr:        *address,
		ReadTimeout: 15 * time.Second,
	}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
			svc.Shutdown(context.Background())
		}
	}()

	psc.PSubscribe("tracks.*")
	go dataReader(psc, b)

	log.Fatal(svc.ListenAndServe())
}
