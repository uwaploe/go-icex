package main

const HOMEPAGE = `
<html lang="en">
<head>
    <title>Target Grid Positions</title>
    <link rel="stylesheet" href="/static/css/style.css"
      type="text/css"
      media="screen" />
    <link rel="stylesheet" href="/static/css/normalize.css"
        type="text/css" media="screen">
    <link rel="stylesheet" href="/static/css/main.css"
          type="text/css" media="screen">
    <script>
      var require = {
          baseUrl: '/static/js'
      };
    </script>
</head>
<body>
<table id="fixes" class="bordered">
  <thead>
  <tr>
    <th>Target</th>
    <th title="Target X in yards">X (yds)</th>
    <th title="Target Y in yards">Y (yds)</th>
    <th title="Fix time UTC">Time</th>
  </tr>
  </thead>
  <tbody>
  <tr id="platform-1">
    <td>PLATFORM 1</td>
    <td class="x-pos"></td>
    <td class="y-pos"></td>
    <td class="tstamp"></td>
  </tr>
  <tr id="platform-2">
    <td>PLATFORM 2</td>
    <td class="x-pos"></td>
    <td class="y-pos"></td>
    <td class="tstamp"></td>
  </tr>
  <tr id="platform-3">
    <td>PLATFORM 3</td>
    <td class="x-pos"></td>
    <td class="y-pos"></td>
    <td class="tstamp"></td>
  </tr>
  <tr id="torpedo-1">
    <td>TORPEDO</td>
    <td class="x-pos"></td>
    <td class="y-pos"></td>
    <td class="tstamp"></td>
  </tr>
  </tbody>
</table>
</body>
<script src="/static/js/lib/require.js"></script>
<script type="text/javascript">
// Ensure that the require.js configuration is loaded before we try to
// load any modules.
require(["/static/js/app-main.js"],
  function() {
    require(["app"], function(app) {
        app.readStream("/stream");
    });
});
</script>
`
